
# QR

Generate QR Codes suitable for printing on the Dymo LabelManager PNP
label maker.

## Notes

### iPhone

The iPhone Camera and Control Center QR Code Reader apps have difficulty
reading QR Codes this small.  The phone needs to be held very still, in
good lighting (for good black/white contrast), and as close as possible
(while still keeping the QR Code in focus).

The QR Code Reader app seems to have an easier time reading small QR
Codes than the Camera app.  Also, the QR Code Reader app allows for the
LED flashlight to be turned on.  This can aid in improving contrast in
low-light situations.

## Setup

### CUPS

On Linux Mint, install the "printer-driver-dymo package".

In CUPS "Printer Properties" -> "Printer Options", set:

* Label Width: 12mm (1/2")
* Media Size: 12mm (1/2") Continuous
* Halftoning: Nonlinear Dithering
* Continuous Paper: Enabled

Set "Halftoning" to "Nonlinear Dithering" in CUPS "Set Print Options" area.

## Generating and printing

```
$ ./qr -m make -o model -s serial
lowswaplab.com/qr/92768E
92768E.png
$ lpq
LabelManager-PnP is ready
no entries
$ lpr -P LabelManager-PnP -o media=w35h144 92768E.png
```

## URL

https://gitlab.com/lowswaplab/qr

## Copyright

Copyright 2018-2023 Low SWaP Lab [lowswaplab.com](https://lowswaplab.com/)

