#!/usr/bin/env python3

# QR
# https://gitlab.com/lowswaplab/qr
# Copyright 2018-2023 Low SWaP Lab lowswaplab.com

import argparse
import qrcode
import random
from PIL import ExifTags, Image, ImageFont, ImageDraw
import pyexiv2

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--make", nargs="?", default="",
  help="Make of device (e.g. \"Low SWaP Lab\")")
parser.add_argument("-o", "--model", nargs="?", default="", \
  help="Model of device (e.g. \"DA-123\")")
parser.add_argument("-s", "--serial", nargs="?", default="", \
  help="Serial number (or UUID) device (e.g. \"456\")")
args = parser.parse_args()

qr = qrcode.QRCode(
  version=1,
  error_correction=qrcode.constants.ERROR_CORRECT_L,
  box_size=1,
  border=1
  )

code = "%06X" % random.randint(0, 0xFFFFFF-1)
#url = "lowswaplab.com/qr"
url = "lowswaplab.com/qr/" + code
text1 = "lowswaplab.com"
text2 = "" + code
fname = code + ".png"

font1 = \
  ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 10)
font2 = \
  ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 20)

qr.add_data(url)
qr.make(fit=True)

imgQR = qr.make_image(fill_color="white", back_color="black")

img = Image.new("1", (115, 30), "white")
img.paste(imgQR, (0, 2))

drw = ImageDraw.Draw(img)
drw.text(xy=(28,1), text=text1, font=font1, fill="#000")
drw.text(xy=(28,9), text=text2, font=font2, fill="#000")

img = img.transpose(Image.ROTATE_270)

img.save(fname)

img = pyexiv2.Image(fname)
img.clear_exif()
img.modify_exif({"Exif.Photo.UserComment": \
  args.make + " " + args.model + " " + args.serial})
img.modify_xmp({"Xmp.dc.LSL.Make": args.make})
img.modify_xmp({"Xmp.dc.LSL.Model": args.model})
img.modify_xmp({"Xmp.dc.LSL.Serial": args.serial})
img.close()

print(url)
print(fname)
print("lpr -P LabelManager-PnP -o media=w35h144", fname)

